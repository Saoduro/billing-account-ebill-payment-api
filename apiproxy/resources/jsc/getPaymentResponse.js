var response = context.getVariable("soapResponseGetPayment.body");
if (typeof response != 'undefined') {
    var responseBody = JSON.parse(response);
    var responseCode = responseBody.applicationError.code;
    var paymentElement = responseBody.payments.EBillPaymentElement;
    var checkDigitId = responseBody.accountNumber.toString();

    var responseObject = {};
    var billingAccounts = [];
    var finalObject = {};

    if (responseCode == 1) {
        context.setVariable('response.status.code', 200);
        var correlationId = context.getVariable("correlationId");

        // map correlationId
        if (responseBody.requestID != "NULL") {
            responseObject.correlationId = correlationId;
        } else {
            responseObject.correlationId = "";
        }

        // map checkDigitBillingId
        if (responseBody.accountNumber != "NULL") {
            finalObject.checkDigitBillingId = checkDigitId;
        } else {
            finalObject.checkDigitBillingId = "";
        }

        //map payment
        var paymentsArray = [];
        

        // map confirmationReceiptId
        if(paymentElement!=null){
        for(var i=0; i < paymentElement.length; i++){
            var paymentObject = {};
            var bankAccount = {};
            bankAccount.id=paymentElement[i].EBillBankAccountElement.id;
            bankAccount.nickname=paymentElement[i].EBillBankAccountElement.nickname;
            bankAccount.financialInstitute=paymentElement[i].EBillBankAccountElement.financialInstituteName;
            bankAccount.type=paymentElement[i].EBillBankAccountElement.type;
        
            paymentObject.amount=paymentElement[i].amount.value.toFixed(2).toString();
            paymentObject.datePosted=paymentElement[i].datePosted;
            paymentObject.dateTimeReceived=paymentElement[i].dateReceived;
            paymentObject.method=paymentElement[i].methodId;
            paymentObject.methodDescription=paymentElement[i].methodDescription;
            paymentObject.status=paymentElement[i].status;
            paymentObject.statusDescription=paymentElement[i].statusDescription;
            paymentObject.confirmationReceiptId = paymentElement[i].receiptId;
            paymentObject.acceptedReceiptId = paymentElement[i].receiptId;
            paymentObject.isModifiable="true";
            paymentObject.bankAccount=bankAccount;
            paymentsArray.push(paymentObject);
        }
        }
        
        finalObject.payment = paymentsArray;

        billingAccounts.push(finalObject);
        responseObject.billingAccounts = billingAccounts;

    } else {
        var responseMessage = responseBody.applicationError.logMessage;
        var responsetype = responseBody.applicationError.type;
        context.setVariable("isValidResponse", false);
        context.setVariable("responseMessage", responseMessage);
        context.setVariable("responsetype", responsetype);
    }


    context.setVariable("finalResponse", JSON.stringify(responseObject));


}