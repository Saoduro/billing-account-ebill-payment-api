 var response = context.getVariable("soapResponse.body");
if (typeof response != 'undefined') {
    var responseBody = JSON.parse(response);
    var responseCode = responseBody.applicationError.code;
    var paymentElement = responseBody.payments.EBillPaymentElement;
    var checkDigitId = responseBody.accountNumber.toString();

    var responseObject = {};
    var billingAccounts = [];
    var finalObject = {};

    if (responseCode == 50) {
        context.setVariable('response.status.code', 201);
        var correlationId = context.getVariable("correlationId");

        // map correlationId
        if (responseBody.requestID != "NULL") {
            responseObject.correlationId = correlationId;
        } else {
            responseObject.correlationId = "";
        }

        // map billingId
        if (responseBody.accountNumber != "NULL") {
            finalObject.billingId = checkDigitId.substr(4);
            finalObject.billingId = finalObject.billingId.substring(0, finalObject.billingId.length - 2);
        } else {
            finalObject.billingId = "";
        }

        // map checkDigitBillingId
        if (responseBody.accountNumber != "NULL") {
            finalObject.checkDigitBillingId = checkDigitId;
        } else {
            finalObject.checkDigitBillingId = "";
        }

        //map payment
        var paymentObject = {};

        // map confirmationReceiptId
        if (paymentElement.receiptId != "NULL") {
            paymentObject.confirmationReceiptId = paymentElement.receiptId;
        } else {
            paymentObject.confirmationReceiptId = "";
        }


        finalObject.payment = paymentObject;

        billingAccounts.push(finalObject);
        responseObject.billingAccounts = billingAccounts;

    } else {
        var responseMessage = responseBody.applicationError.logMessage;
        var responsetype = responseBody.applicationError.type;
        context.setVariable("isValidResponse", false);
        context.setVariable("responseMessage", responseMessage);
        context.setVariable("responsetype", responsetype);
    }


    context.setVariable("finalResponse", JSON.stringify(responseObject));


}