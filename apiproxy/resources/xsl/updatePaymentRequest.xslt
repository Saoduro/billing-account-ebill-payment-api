<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xslt="http://xml.apache.org/xslt">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" xslt:indent-amount="2"/>
	<xsl:strip-space elements="*"/>
	<xsl:param name="usernameTokenId" select="''"/>
	<xsl:param name="username" select="''"/>
	<xsl:param name="password" select="''"/>
	<xsl:param name="requestId" select="''"/>

	<xsl:template match="/soap:Envelope">
		<soap:Envelope>
			<soap:Header>
				<xsl:choose>
					<xsl:when test="soap:Header">
						<xsl:apply-templates select="soap:Header"/>
					</xsl:when>
					<xsl:otherwise>
						<wsse:Security>
							<xsl:call-template name="injectUsernameToken"/>
						</wsse:Security>
					</xsl:otherwise>
				</xsl:choose>
			</soap:Header>
			<soap:Body>
				<xsl:apply-templates select="soap:Body"/>
			</soap:Body>
		</soap:Envelope>
	</xsl:template>
	<xsl:template match="soap:Body">
		<ns2:addPayment xmlns:ns2="http://services.oam.xcel.com">
			<requestElement>
				<endUser>
					<endUserID>
					    <xsl:value-of select="./*[local-name() = 'addPayment']/*[local-name() = 'billingAccounts']/*[local-name() = 'email']"/>
					</endUserID>
					<endUserType>P</endUserType>
				</endUser>
				<requestID>
					<xsl:value-of select="$requestId"/>
				</requestID>
				<accountNumber>
					<xsl:value-of select="./*[local-name() = 'addPayment']/*[local-name() = 'billingAccounts']/*[local-name() = 'checkDigitBillingId']"/>
				</accountNumber>
				<payment>
					<amount>
						<value>
							<xsl:value-of select="./*[local-name() = 'addPayment']/*[local-name() = 'billingAccounts']/*[local-name() = 'payment']/*[local-name() = 'amount']"/>
						</value>
					</amount>
					<datePosted>
						<xsl:value-of select="./*[local-name() = 'addPayment']/*[local-name() = 'billingAccounts']/*[local-name() = 'payment']/*[local-name() = 'datePosted']"/>
					</datePosted>
					<dateReceived/>
					<id/>
					<methodId>eBill Payment</methodId>
					<receiptId/>
					<status>S</status>
					<statusDescription>Submitted</statusDescription>
					<EBillBankAccountElement>
						<id>
							<xsl:value-of select="./*[local-name() = 'addPayment']/*[local-name() = 'billingAccounts']/*[local-name() = 'bankAccount']/*[local-name() = 'id']"/>
						</id>
						<nickname>
							<xsl:value-of select="./*[local-name() = 'addPayment']/*[local-name() = 'billingAccounts']/*[local-name() = 'bankAccount']/*[local-name() = 'nickname']"/>
						</nickname>
						<financialInstituteName>
							<xsl:value-of select="./*[local-name() = 'addPayment']/*[local-name() = 'billingAccounts']/*[local-name() = 'bankAccount']/*[local-name() = 'financialInstitute']"/>
						</financialInstituteName>
					</EBillBankAccountElement>
					<memo>Test</memo>
				</payment>
			</requestElement>
		</ns2:addPayment>
	</xsl:template>
	<xsl:template match="soap:Header">
		<xsl:choose>
			<xsl:when test='wsse:Security'>
				<xsl:apply-templates select="*"/>
			</xsl:when>
			<xsl:otherwise>
				<wsse:Security>
					<xsl:call-template name="injectUsernameToken"/>
				</wsse:Security>
				<xsl:copy-of select="*"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="soap:Header/wsse:Security/wsse:UsernameToken"/>
	<xsl:template name='injectUsernameToken'>
		<wsse:UsernameToken>
			<xsl:attribute name="Id"><xsl:value-of select="$usernameTokenId"/></xsl:attribute>
			<wsse:Username>
				<xsl:value-of select="$username"/>
			</wsse:Username>
			<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">
				<xsl:value-of select="$password"/>
			</wsse:Password>
		</wsse:UsernameToken>
	</xsl:template>
</xsl:stylesheet>
